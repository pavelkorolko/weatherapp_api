package PDF;

import com.lowagie.text.*;
import com.lowagie.text.pdf.PdfWriter;

import java.io.*;

public class Pdf {
    private static String filename;
    public Pdf(String fileName){
        this.filename = fileName;
    }

    public String getAdjustment(){
        try{
            this.getNameWithoutExtension();
            Document pdfDoc = new Document(PageSize.A4);
            PdfWriter.getInstance(pdfDoc, new FileOutputStream(filename + ".pdf"))
                    .setPdfVersion(PdfWriter.PDF_VERSION_1_7);
            pdfDoc.open();
            Font myfont = new Font();
            myfont.setStyle(Font.NORMAL);
            myfont.setSize(11);
            pdfDoc.add(new Paragraph("\n"));
            BufferedReader br = new BufferedReader(new FileReader(filename + ".txt"));
            String strLine;
            while ((strLine = br.readLine()) != null) {
                Paragraph para = new Paragraph(strLine + "\n", myfont);
                para.setAlignment(Element.ALIGN_LEFT);
                pdfDoc.add(para);
            }
            pdfDoc.close();
            br.close();

        }catch(IOException | DocumentException e){
            e.printStackTrace();
        }
        return filename + ".pdf";
    }
    public void getNameWithoutExtension(){
        int index = filename.indexOf(".");
        this.filename = filename.substring(0,index);
    }
}
