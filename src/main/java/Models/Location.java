package Models;

import Models.WeatherParameters.WeatherParameter;
import jakarta.persistence.*;
import java.util.Set;

@Entity
public class Location {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int UUID;
    @Column(unique = true, nullable = false)
    private double longitude;
    @Column(unique = true, nullable = false)
    private double latitude;
    @Column(nullable = false)
    private String city;
    private String region;
    @Column(nullable = false)
    private String country;

    @OneToMany
    private Set<WeatherParameter> weatherParameters;

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getCity() {return city;}

    public void setCity(String city) {this.city = city;}

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setWeatherParameters(Set<WeatherParameter> weatherParameters) {
        this.weatherParameters = weatherParameters;
    }

    public Location(){}

    public Location(String city, String country, String region, double longitude, double latitude){
        this.longitude = longitude;
        this.latitude = latitude;
        this.city = city;
        this.region = region;
        this.country = country;
    }
}
