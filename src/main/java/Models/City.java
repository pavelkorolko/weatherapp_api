package Models;

import jakarta.persistence.*;

public class City {
    private int UUID;
    private String city;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
