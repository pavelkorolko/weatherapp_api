package Models.WeatherParameters;

import jakarta.persistence.*;

@Entity
@Table(name = "night_time")
public class NightTime {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int UUID;
    @Column
    private String iconPhraseNight;
    private boolean hasPrecipitationNight;

    @OneToOne(mappedBy = "nightTime")
    private WeatherParameter parameters;


    public NightTime(String iconPhrase, boolean hasPrecipitation) {
        this.iconPhraseNight = iconPhrase;
        this.hasPrecipitationNight = hasPrecipitation;
    }

    public String getIconPhraseNight() {
        return iconPhraseNight;
    }

    public String precipitationReply(){
        if(hasPrecipitationNight){
            return "raining";
        }else{
            return "no raining";
        }
    }
}
