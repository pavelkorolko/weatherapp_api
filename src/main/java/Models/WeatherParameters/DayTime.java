package Models.WeatherParameters;

import jakarta.persistence.*;

@Entity
@Table(name = "day_time")
public class DayTime {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int UUID;
    private String iconPhraseDay;
    private boolean hasPrecipitationDay;
    @OneToOne(mappedBy = "dayTime")
    private WeatherParameter parameters;

    public DayTime(String iconPhrase, boolean hasPrecipitation) {
        this.iconPhraseDay = iconPhrase;
        this.hasPrecipitationDay = hasPrecipitation;
    }

    public String getIconPhraseDay() {
        return iconPhraseDay;
    }

    public String precipitationReply(){
        if(hasPrecipitationDay){
            return "raining";
        }else{
            return "no raining";
        }
    }
}
