package Models.WeatherParameters;

import AccessKeys.AccessKey;
import Entry.Main;
import Models.Location;
import jakarta.persistence.*;
import top.jfunc.json.JsonArray;
import top.jfunc.json.JsonException;
import top.jfunc.json.JsonObject;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "weatherParameters")
public class WeatherParameter {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int UUID;
    @ManyToOne
    @JoinColumn(name = "location_id", nullable = false)
    private Location location;
    @Column(nullable = false)
    private LocalDate date;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "local_temp_id")
    private LocalTemp localTemp;
    private int pressure;
    private int humidity;
    private String windDirection;
    private double windSpeed;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "day_time_id")
    private DayTime dayTime;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "night_time_id")
    private NightTime nightTime;

    public void setLocation(Location current) {

        this.location = current;
    }

    public WeatherParameter() {
    }

    //for future weather
    public WeatherParameter(LocalDate date, LocalTemp localTemp, DayTime dayTime, NightTime nightTime) {
        this.date = date;
        this.localTemp = localTemp;
        this.dayTime = dayTime;
        this.nightTime = nightTime;
    }

    //for current weather
    public WeatherParameter(LocalDate date, LocalTemp localTemp, int pressure, int humidity, String windDirection, double windSpeed) {
        this.date = date;
        this.localTemp = localTemp;
        this.pressure = pressure;
        this.humidity = humidity;
        this.windDirection = windDirection;
        this.windSpeed = windSpeed;
    }

    public static List<WeatherParameter> retrieveForecastData(Location currentLocation, String provider, JsonObject obj, long foreCastDays, String unit) {
        List<WeatherParameter> listOfParamsByDay = new ArrayList<>();
        String curTempUnit;
        if (unit.equalsIgnoreCase("F")) {
            curTempUnit = "Fahrenheit";
        } else {
            curTempUnit = "Celsius";
        }

        if (AccessKey.one.getProvider().equals(provider)) {
            try {
                JsonObject currentForeCastAPI1 = obj.getJsonObject("current");

                //WeatherStack has info about temperature, pressure, wind direction, humidity, wind speed
                //retrieving temp is by default from JSON is Celsius
                int currentTemp = currentForeCastAPI1.getInteger("temperature");
                int currentPressure = currentForeCastAPI1.getInteger("pressure");
                String currentWindDir = currentForeCastAPI1.getString("wind_dir");
                int humidity = currentForeCastAPI1.getInteger("humidity");
                double currentWindSpeed = currentForeCastAPI1.getDouble("wind_speed");
                String date = obj.getJsonObject("location").getString("localtime");
                LocalDate convertedDate = LocalDate.parse(Main.getTruncated(date, " "));
                LocalTemp curTemp;
                if (curTempUnit.equalsIgnoreCase("Fahrenheit")) {
                    curTemp = new LocalTemp(curTempUnit, 0, currentTemp);
                    curTemp.convertFromCelsiusToFahrenheit();
                } else {
                    curTemp = new LocalTemp(curTempUnit, 0, currentTemp);
                }
                WeatherParameter temp = new WeatherParameter(convertedDate, curTemp, currentPressure, humidity, currentWindDir, currentWindSpeed);
                temp.setLocation(currentLocation);
                listOfParamsByDay.add(temp);
            } catch (JsonException e) {
                System.out.println("API calls have been exceeded! Check subscription plan of " + AccessKey.one.getProvider());
                return null;
            }
        } else if (AccessKey.two.getProvider().equals(provider)) {
            try {
                JsonObject currentForeCastAPI2 = obj.getJsonObject("main");

                //OpenWeatherMap has info about temperature, pressure, wind direction, humidity, wind speed
                int currentPressure = currentForeCastAPI2.getInteger("pressure");
                String currentWindDir = obj.getJsonObject("wind").getInteger("deg").toString();
                int humidity = currentForeCastAPI2.getInteger("humidity");
                double currentWindSpeed = obj.getJsonObject("wind").getDouble("speed");
                //retrieving tempMin/tempMax is by default from JSON is Celsius
                int tempMin = currentForeCastAPI2.getDouble("temp_min").intValue();
                int tempMax = currentForeCastAPI2.getDouble("temp_max").intValue();
                LocalTemp curTemp;
                if (curTempUnit.equalsIgnoreCase("Fahrenheit")) {
                    curTemp = new LocalTemp(curTempUnit, tempMin, tempMax);
                    curTemp.convertFromCelsiusToFahrenheit();
                } else {
                    curTemp = new LocalTemp(curTempUnit, tempMin, tempMax);
                }
                WeatherParameter temp = new WeatherParameter(null, curTemp, currentPressure, humidity, currentWindDir, currentWindSpeed);
                temp.setLocation(currentLocation);
                listOfParamsByDay.add(temp);
            } catch (JsonException e) {
                System.out.println("API calls have been exceeded! Check subscription plan of " + AccessKey.two.getProvider());
                return null;
            }

        } else if (AccessKey.three.getProvider().equals(provider)) {
            try {
                JsonArray dailyForeCasts = obj.getJsonArray("DailyForecasts");

                //AccuWeather has info about date, temperature, precipitations during daytime/nighttime, clouds
                LocalDate parsedDate;
                double tempMin;
                double tempMax;
                String tempUnits;
                String iconPhraseDay;
                String iconPhraseNight;
                boolean hasPrecipitationDay;
                boolean hasPrecipitationNight;

                for (int i = 1; i < foreCastDays + 1; i++) {
                    JsonObject current = dailyForeCasts.getJsonObject(i);
                    parsedDate = LocalDate.parse(Main.getTruncated((current.getString("Date")), "T"));
                    //retrieving temp is by default from JSON is Fahrenheit
                    tempMin = current.getJsonObject("Temperature").getJsonObject("Minimum").getDouble("Value");
                    tempMax = current.getJsonObject("Temperature").getJsonObject("Maximum").getDouble("Value");
                    tempUnits = current.getJsonObject("Temperature").getJsonObject("Maximum").getString("Unit");
                    if (!tempUnits.equalsIgnoreCase(curTempUnit.substring(0, 1))) {
                        curTempUnit = "Celsius";
                    }

                    LocalTemp curTemp;
                    if (curTempUnit.equalsIgnoreCase("Celsius")) {
                        curTemp = new LocalTemp(curTempUnit, tempMin, tempMax);
                        curTemp.convertFromFahrenheitToCelsius();
                    } else {
                        curTemp = new LocalTemp(curTempUnit, tempMin, tempMax);
                    }
                    iconPhraseDay = current.getJsonObject("Day").getString("IconPhrase");
                    hasPrecipitationDay = current.getJsonObject("Day").getBoolean("HasPrecipitation");
                    iconPhraseNight = current.getJsonObject("Night").getString("IconPhrase");
                    hasPrecipitationNight = current.getJsonObject("Day").getBoolean("HasPrecipitation");
                    DayTime dayTime = new DayTime(iconPhraseDay, hasPrecipitationDay);
                    NightTime nightTime = new NightTime(iconPhraseNight, hasPrecipitationNight);
                    WeatherParameter temp = new WeatherParameter(parsedDate, curTemp, dayTime, nightTime);
                    temp.setLocation(currentLocation);
                    listOfParamsByDay.add(temp);
                }
            } catch (JsonException e) {
                System.out.println("API calls have been exceeded! Check subscription plan of " + AccessKey.three.getProvider());
                return null;
            }
        } else {
            return null;
        }
        return listOfParamsByDay;
    }

    public String toStringForecast(String provider, DateTimeFormatter dtf) {
        if (AccessKey.three.getProvider().equals(provider)) {
            return "-----------------------------------------------" +
                    "\nDay: " + dtf.format(date) + "\nTemperature:\n" +
                    "max: " + String.format("%5.2f", localTemp.getMaxValueTemp()) + localTemp.getUnit() +
                    "\nmin: " + String.format("%5.2f", localTemp.getMinValueTemp()) + localTemp.getUnit() +
                    "\nDuring day time will be " + dayTime.getIconPhraseDay().toLowerCase() + " and " + dayTime.precipitationReply() + ".\n" +
                    "During night time will be " + nightTime.getIconPhraseNight().toLowerCase() + " and " + nightTime.precipitationReply() + ".";
        } else {
            return "-----------------------------------------------" +
                    "\nToday: " + "\nTemperature: " +
                    +localTemp.getMaxValueTemp() + localTemp.getUnit() +
                    ".\nCurrent air pressure " + pressure + "mmHg and air humidity " + humidity + "%.\n" +
                    "During today wind will be directed " + windDirection + "(degree) with speed " + windSpeed + "m/s.";
        }
    }

    public static WeatherParameter getAveragedValues(WeatherParameter par1, WeatherParameter par2) {
        if(par1 != null && par2 != null){
            double averageMinTemp = (par1.localTemp.getMinValueTemp() + par2.localTemp.getMinValueTemp()) / 2;
            double averageMaxTemp = (par1.localTemp.getMaxValueTemp() + par2.localTemp.getMaxValueTemp()) / 2;
            LocalTemp averageTemp = new LocalTemp(par1.localTemp.getUnit(), averageMinTemp, averageMaxTemp);
            int averagePressure = (par1.pressure + par2.pressure) / 2;
            int averageHumidity = (par1.humidity + par2.humidity) / 2;
            double averageWindSpeed = (par1.windSpeed + par2.windSpeed) / 2;
            return new WeatherParameter(par1.date, averageTemp, averagePressure, averageHumidity, par2.windDirection, averageWindSpeed);
        }else if(par2 == null){
            return par1;
        }else{
            return par2;
        }
    }
}


