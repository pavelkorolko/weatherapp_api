package Models.WeatherParameters;

import jakarta.persistence.*;

@Entity
@Table(name = "local_temp")
public class LocalTemp {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int UUID;
    private String unit;
    private double minValueTemp;
    private double maxValueTemp;
    @OneToOne(mappedBy = "localTemp")
    private WeatherParameter parameters;

    public LocalTemp(String unit, double minValue, double maxValue) {
        this.unit = unit;
        this.minValueTemp = minValue;
        this.maxValueTemp = maxValue;
    }

    public String getUnit() {
        return unit;
    }

    public double getMinValueTemp() {
        return minValueTemp;
    }

    public double getMaxValueTemp() {
        return maxValueTemp;
    }

    public void convertFromCelsiusToFahrenheit(){
        minValueTemp = minValueTemp * 1.8 + 32;
        maxValueTemp = maxValueTemp * 1.8 + 32;
    }
    public void convertFromFahrenheitToCelsius(){
        minValueTemp = (minValueTemp -32)/1.8;
        maxValueTemp = (maxValueTemp -32)/1.8;
    }
}
