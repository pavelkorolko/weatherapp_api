package Entry;

import AccessKeys.AccessKey;
import Models.Location;
import Models.WeatherParameters.WeatherParameter;
import PDF.Pdf;
import Repositories.LocationRepository;
import Repositories.WeatherParamsRepository;
import WeatherAPIConnection.HTTPWeather;
import org.javatuples.Quartet;
import top.jfunc.json.JsonArray;
import top.jfunc.json.JsonException;
import top.jfunc.json.JsonObject;

import java.awt.*;
import java.io.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoUnit;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        boolean flag = true;
        LocationRepository locationDB = new LocationRepository();
        WeatherParamsRepository paramsRepository = new WeatherParamsRepository();
        do {
            Scanner scn = new Scanner(System.in);
            System.out.println("\nWelcome to WeatherLady!");
            System.out.println("Menu:");
            System.out.println("Press -1- -------> Add location to the database");
            System.out.println("Press -2- -------> Display currently added locations");
            System.out.println("Press -3- -------> Observe weather for location");
            System.out.println("Press -4- -------> Exit");
            System.out.print("Type here: ");

            int reply;
            if (scn.hasNextInt()) {
                reply = scn.nextInt();
                scn.nextLine();
            } else {
                System.out.println("Invalid data!");
                return;
            }

            switch (reply) {
                case 1:
                    String city;
                    String region;
                    String country;
                    double latitude;
                    double longitude;

                    //city validation
                    while (true) {
                        System.out.print("Provide city: ");
                        String tempCity = scn.nextLine();
                        if (tempCity.length() != 0 && validate(tempCity)) {
                            city = tempCity;
                            break;
                        } else {
                            System.out.println("Invalid input! Try once more!");
                        }
                    }

                    //region validation
                    System.out.println("Do you want to provide region?");
                    System.out.print("Type Yes/No: ");
                    String answer = scn.nextLine();
                    if (answer.equalsIgnoreCase("yes")) {
                        System.out.print("Provide region: ");
                        region = scn.nextLine();
                    } else {
                        region = null;
                    }

                    //country validation
                    while (true) {
                        System.out.print("Provide country: ");
                        String tempCountry = scn.nextLine();
                        if (tempCountry.length() != 0 && validate(tempCountry)) {
                            country = tempCountry;
                            break;
                        } else {
                            System.out.println("Invalid input! Try once more!");
                        }
                    }

                    //longitude validation
                    while (true) {
                        System.out.print("Provide longitude: ");
                        try {
                            longitude = Double.parseDouble(scn.next());
                            break;
                        } catch (NumberFormatException ignore) {
                            System.out.println("Invalid input! Try once more!");
                        }
                    }

                    //latitude validation
                    while (true) {
                        System.out.print("Provide latitude: ");
                        try {
                            latitude = Double.parseDouble(scn.next());
                            break;
                        } catch (NumberFormatException ignore) {
                            System.out.println("Invalid input! Try once more!");
                        }
                    }

                    //adding location after valid input to table
                    locationDB.addLocation(city, country, region, longitude, latitude);
                    break;
                case 2:
                    //getting all locations from table
                    List<Location> locations = locationDB.getAll();
                    if (locations.size() > 0) {
                        System.out.println("Do you want to print on console?");
                        System.out.print("Type Yes/No: ");
                        if (scn.nextLine().equalsIgnoreCase("no")) {
                            System.out.println("Do you want to print on pdf?");
                            System.out.print("Type Yes/No: ");
                            if (scn.nextLine().equalsIgnoreCase("yes")) {
                                File newFile = new File("locations.txt");
                                printValuesToFile(locations, newFile);
                                try {
                                    //initializing pdf document and saving into project's folder
                                    Pdf pdf = new Pdf("locations.txt");
                                    String savedPdf = pdf.getAdjustment();

                                    //getting to screen
                                    File myFile = new File(savedPdf);
                                    Desktop.getDesktop().open(myFile);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        } else {
                            //getting to console
                            printValuesToFile(locations, null);
                        }
                    } else {
                        System.out.println("There is no data for records!");
                    }
                    break;
                case 3:
                    System.out.print("City: ");
                    String requestedCity = scn.nextLine();

                    //first API source(weatherStack)
                    HTTPWeather connection1 = new HTTPWeather(AccessKey.one, requestedCity);
                    JsonObject jsonData1 = connection1.getJSON();

                    //second API source(openWeatherMap)
                    HTTPWeather connection2 = new HTTPWeather(AccessKey.two, requestedCity);
                    JsonObject jsonData2 = connection2.getJSON();

                    //json writing current weather data to file
                    try {
                        writeJSONFile(jsonData1, "currentData1.json");
                        writeJSONFile(jsonData2, "currentData2.json");
                    } catch (Exception e) {
                        return;
                    }

                    //adding info from jsonData1 only if city does not exist already in db
                    if (!locationDB.ifExists(requestedCity)) {
                        System.out.println("Sorry, but city " + requestedCity + " is not found in added location's search preferences!");
                        System.out.println("Do you want to add?");
                        System.out.print("Type here Yes/No: ");
                        if (scn.nextLine().equalsIgnoreCase("No")) {
                            System.out.println("Weather query can not be done!");
                            return;
                        } else {
                            System.out.println("Values of country/region/latitude/longitude are automatically added!");
                            Quartet<String, String, Double, Double> requiredDataFromJsonData = retrieveDataFromJSONObject(jsonData1);
                            locationDB.addLocation(requestedCity, requiredDataFromJsonData.getValue0(), requiredDataFromJsonData.getValue1(), requiredDataFromJsonData.getValue2(), requiredDataFromJsonData.getValue3());
                        }
                    }

                    //retrieving location object from db
                    Location currentLocation = locationDB.getLocation(requestedCity);

                    //retrieving current date
                    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
                    LocalDate currentDate = LocalDate.now();
                    System.out.println("\nToday is: " + dtf.format(currentDate) + ". Choose forecast time below!");
                    System.out.print("Want to see temperature in Celsius or Fahrenheit? ");
                    String tempUnit;

                    while (true) {
                        System.out.print("Type here C or F respectively: ");
                        tempUnit = scn.nextLine();
                        if (tempUnit.equalsIgnoreCase("C") || tempUnit.equalsIgnoreCase("F")) {
                            break;
                        } else {
                            System.out.println("Invalid input! Try once more!");
                        }
                    }

                    boolean flagContinue = false;
                    do {
                        System.out.println("Press -1- -------> For today");
                        System.out.println("Press -2- -------> For future");
                        System.out.print("Type here: ");

                        if (scn.hasNextInt()) {
                            reply = scn.nextInt();
                            scn.nextLine();
                            flag = false;
                        } else {
                            System.out.println("Invalid input! Try once more!");
                            scn.nextLine();
                        }

                        if (!flag) {
                            switch (reply) {
                                case 1:
                                    //getting current weather(1 day) from saved jsons

                                    //from currentData1.json
                                    List<WeatherParameter> currentWeatherAPI1 = WeatherParameter.retrieveForecastData(currentLocation, AccessKey.one.getProvider(), jsonData1, 1, tempUnit);

                                    //from currentData2.json
                                    List<WeatherParameter> currentWeatherAPI2 = WeatherParameter.retrieveForecastData(currentLocation, AccessKey.two.getProvider(), jsonData2, 1, tempUnit);

                                    //averaging values retrieved from both APIs
                                    if (currentWeatherAPI1 != null && currentWeatherAPI2 != null) {
                                        for (int i = 0; i < currentWeatherAPI1.size(); i++) {
                                            WeatherParameter averaged = WeatherParameter.getAveragedValues(currentWeatherAPI1.get(i), currentWeatherAPI2.get(i));
                                            averaged.setLocation(currentLocation);
                                            System.out.println(averaged.toStringForecast(AccessKey.one.getProvider(), dtf));

                                            //adding averaged retrieved weatherParameter object to location object(making connection between tables)
                                            Set<WeatherParameter> currentParameters = new HashSet<>();
                                            currentParameters.add(averaged);

                                            //updating location object via db
                                            locationDB.updateLocationBy(currentParameters, requestedCity);

                                            //adding parameters to db
                                            paramsRepository.addParametersForCurrent(averaged);
                                        }
                                    }

                                    System.out.print("Want to continue? If Yes, press Enter! If No, type any symbol and press Enter ");
                                    flagContinue = ContinueConsoleOrNot(scn);
                                    break;
                                case 2:
                                    try {
                                        //getting location key for forecast(accuWeather)
                                        HTTPWeather connection3 = new HTTPWeather(AccessKey.three, requestedCity);
                                        JsonArray jsonData3 = connection3.getJSONArray();
                                        String locationKey = jsonData3.getJsonObject(0).getString("Key");
                                        connection3.setLocationKey(locationKey);
                                    } catch (NullPointerException e) {
                                        System.out.println("Request can not be done!");
                                        return;
                                    }

                                    long daysInDateFormat = -1;
                                    long daysInNumberFormat = -1;
                                    long finalDays;

                                    do {
                                        System.out.print("Get forecast from current date(in days, max available 4 days or by format DD/MM/YYYY): ");
                                        String inputDays = scn.nextLine();
                                        try {
                                            LocalDate indicatedDate = LocalDate.parse(inputDays, dtf);
                                            daysInDateFormat = ChronoUnit.DAYS.between(currentDate, indicatedDate);
                                            flag = false;
                                        } catch (DateTimeParseException e) {
                                            try {
                                                daysInNumberFormat = Long.parseLong(inputDays);
                                                flag = false;
                                            } catch (NumberFormatException m) {
                                                System.out.println("Invalid input! Try once more!");
                                            }
                                        }
                                    } while (flag);

                                    if (daysInNumberFormat != -1) {
                                        finalDays = daysInNumberFormat;
                                    } else {
                                        finalDays = daysInDateFormat;
                                    }

                                    if (finalDays <= 1 || finalDays > 4) {
                                        finalDays = 1;
                                    }
                                    //free API source for forecast(accuWeather)
                                    HTTPWeather connection = new HTTPWeather(AccessKey.three, requestedCity, finalDays, 0, 0);
                                    JsonObject jsonForecastData = connection.getJSON();

                                    //json writing forecast weather data to file3
                                    try {
                                        writeJSONFile(jsonForecastData, "jsonForecastData.json");
                                    } catch (Exception e) {
                                        return;
                                    }

                                    System.out.println("There is ready forecast for " + finalDays + " day(s)!");
                                    List<WeatherParameter> weatherByDays = WeatherParameter.retrieveForecastData(currentLocation, AccessKey.three.getProvider(), jsonForecastData, finalDays, tempUnit);

                                    //averaging values retrieved from APIs(here from one API)
                                    if (weatherByDays != null) {
                                        Set<WeatherParameter> currentParameters = new HashSet<>();
                                        for (int i = 0; i < weatherByDays.size(); i++) {
                                            WeatherParameter averaged = WeatherParameter.getAveragedValues(weatherByDays.get(i), null);
                                            averaged.setLocation(currentLocation);
                                            System.out.println(weatherByDays.get(i).toStringForecast(AccessKey.three.getProvider(), dtf));

                                            //adding averaged retrieved weatherParameter object to location object(making connection between tables)
                                            currentParameters = new HashSet<>();
                                            currentParameters.add(averaged);

                                            //adding parameters to db
                                            paramsRepository.addParametersForCurrent(averaged);
                                        }
                                        //updating location object via db
                                        locationDB.updateLocationBy(currentParameters, requestedCity);
                                    }

                                    System.out.println("\nWant to continue? If Yes, press Enter! If No, type any symbol and press Enter");
                                    flagContinue = ContinueConsoleOrNot(scn);
                                    break;
                                default:
                                    System.out.println("Number is not correct! Try once more!");
                                    flag = true;
                            }
                        }
                    } while (flag);
                    if (flagContinue) {
                        getStarted(locationDB, paramsRepository);
                    }
                    break;
                case 4:
                    System.exit(0);
                    break;
                default:
                    System.out.println("Press keys from 1 to 4 for next step!");
            }
        } while (flag);
    }

    public static boolean validate(String city) {
        return city.matches("(^[A-Z])([a-z]+){2,}$|[A-Z][a-z]*\\s[A-Z][a-z]*");
    }

    public static void printValuesToFile(List<Location> locations, File file) {
        if (file == null) {
            System.out.printf("%-25s", "City");
            System.out.printf("%-25s", "Country");
            System.out.printf("%-35s", "Region");
            System.out.printf("%-25s", "Longitude");
            System.out.printf("%-25s%n", "Latitude");

            for (Location location : locations) {
                System.out.printf("%-25s", location.getCity());
                System.out.printf("%-25s", location.getCountry());
                System.out.printf("%-35s", location.getRegion());
                System.out.printf("%-25s", location.getLongitude());
                System.out.printf("%-25s%n", location.getLatitude());
            }
        } else {
            try {
                FileWriter writer = new FileWriter(file, false);
                writer.write(String.format("%-25s", "City:"));
                writer.write(String.format("%-25s", "Country:"));
                writer.write(String.format("%-35s", "Region:"));
                writer.write(String.format("%-25s", "Longitude:"));
                writer.write(String.format("%-25s%n", "Latitude:"));
                for (Location location : locations) {
                    writer.write(String.format("%-25s", location.getCity()));
                    writer.write(String.format("%-25s", location.getCountry()));
                    writer.write(String.format("%-35s", location.getRegion()));
                    writer.write(String.format("%-25s", location.getLongitude()));
                    writer.write(String.format("%-25s%n", location.getLatitude()));
                }
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void writeJSONFile(JsonObject obj, String fileName) throws Exception {
        try {
            FileWriter file = new FileWriter("src\\main\\java\\JSONs\\" + fileName);
            file.write(obj.toString());
            file.close();
        } catch (Exception e) {
            System.out.println("Unexpected error! Try entering city correctly!");
            throw e;
        }
    }

    public static void getStarted(LocationRepository locationDB, WeatherParamsRepository parametersDB) {
        boolean flag = true;
        do {
            Scanner scn = new Scanner(System.in);
            System.out.println("\nWelcome to WeatherLady!");
            System.out.println("Menu:");
            System.out.println("Press -1- -------> Add location to the database");
            System.out.println("Press -2- -------> Display currently added locations");
            System.out.println("Press -3- -------> Observe weather for location");
            System.out.println("Press -4- -------> Exit");
            System.out.print("Type here: ");

            int reply;
            if (scn.hasNextInt()) {
                reply = scn.nextInt();
                scn.nextLine();
            } else {
                System.out.println("Invalid data!");
                return;
            }

            switch (reply) {
                case 1:
                    String city;
                    String region;
                    String country;
                    double latitude;
                    double longitude;

                    //city validation
                    while (true) {
                        System.out.print("Provide city: ");
                        String tempCity = scn.nextLine();
                        if (tempCity.length() != 0 && validate(tempCity)) {
                            city = tempCity;
                            break;
                        } else {
                            System.out.println("Invalid input! Try once more!");
                        }
                    }

                    //region validation
                    System.out.println("Do you want to provide region?");
                    System.out.print("Type Yes/No: ");
                    String answer = scn.nextLine();
                    if (answer.equalsIgnoreCase("yes")) {
                        System.out.print("Provide region: ");
                        region = scn.nextLine();
                    } else {
                        region = null;
                    }

                    //country validation
                    while (true) {
                        System.out.print("Provide country: ");
                        String tempCountry = scn.nextLine();
                        if (tempCountry.length() != 0 && validate(tempCountry)) {
                            country = tempCountry;
                            break;
                        } else {
                            System.out.println("Invalid input! Try once more!");
                        }
                    }

                    //longitude validation
                    while (true) {
                        System.out.print("Provide longitude: ");
                        try {
                            longitude = Double.parseDouble(scn.next());
                            break;
                        } catch (NumberFormatException ignore) {
                            System.out.println("Invalid input! Try once more!");
                        }
                    }

                    //latitude validation
                    while (true) {
                        System.out.print("Provide latitude: ");
                        try {
                            latitude = Double.parseDouble(scn.next());
                            break;
                        } catch (NumberFormatException ignore) {
                            System.out.println("Invalid input! Try once more!");
                        }
                    }

                    //adding location after valid input to table
                    locationDB.addLocation(city, country, region, longitude, latitude);
                    break;
                case 2:
                    //getting all locations from table
                    List<Location> locations = locationDB.getAll();
                    if (locations.size() > 0) {
                        System.out.println("Do you want to print on console?");
                        System.out.print("Type Yes/No: ");
                        if (scn.nextLine().equalsIgnoreCase("no")) {
                            System.out.println("Do you want to print on pdf?");
                            System.out.print("Type Yes/No: ");
                            if (scn.nextLine().equalsIgnoreCase("yes")) {
                                File newFile = new File("locations.txt");
                                printValuesToFile(locations, newFile);
                                try {
                                    //initializing pdf document and saving into project's folder
                                    Pdf pdf = new Pdf("locations.txt");
                                    String savedPdf = pdf.getAdjustment();

                                    //getting to screen
                                    File myFile = new File(savedPdf);
                                    Desktop.getDesktop().open(myFile);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        } else {
                            //getting to console
                            printValuesToFile(locations, null);
                        }
                    } else {
                        System.out.println("There is no data for records!");
                    }
                    break;
                case 3:
                    System.out.print("City: ");
                    String requestedCity = scn.nextLine();

                    //first API source(weatherStack)
                    HTTPWeather connection1 = new HTTPWeather(AccessKey.one, requestedCity);
                    JsonObject jsonData1 = connection1.getJSON();

                    //second API source(openWeatherMap)
                    HTTPWeather connection2 = new HTTPWeather(AccessKey.two, requestedCity);
                    JsonObject jsonData2 = connection2.getJSON();

                    //json writing current weather data to file
                    try {
                        writeJSONFile(jsonData1, "currentData1.json");
                        writeJSONFile(jsonData2, "currentData2.json");
                    } catch (Exception e) {
                        return;
                    }

                    //adding info from jsonData1 only if city does not exist already in db
                    if (!locationDB.ifExists(requestedCity)) {
                        System.out.println("Sorry, but city " + requestedCity + " is not found in added location's search preferences!");
                        System.out.println("Do you want to add?");
                        System.out.print("Type here Yes/No: ");
                        if (scn.nextLine().equalsIgnoreCase("No")) {
                            System.out.println("Weather query can not be done!");
                            return;
                        } else {
                            System.out.println("Values of country/region/latitude/longitude are automatically added!");
                            Quartet<String, String, Double, Double> requiredDataFromJsonData = retrieveDataFromJSONObject(jsonData1);
                            locationDB.addLocation(requestedCity, requiredDataFromJsonData.getValue0(), requiredDataFromJsonData.getValue1(), requiredDataFromJsonData.getValue2(), requiredDataFromJsonData.getValue3());
                        }
                    }

                    //retrieving location object from db
                    Location currentLocation = locationDB.getLocation(requestedCity);

                    //retrieving current date
                    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
                    LocalDate currentDate = LocalDate.now();
                    System.out.println("\nToday is: " + dtf.format(currentDate) + ". Choose forecast time below!");
                    System.out.print("Want to see temperature in Celsius or Fahrenheit? ");
                    String tempUnit;

                    while (true) {
                        System.out.print("Type here C or F respectively: ");
                        tempUnit = scn.nextLine();
                        if (tempUnit.equalsIgnoreCase("C") || tempUnit.equalsIgnoreCase("F")) {
                            break;
                        } else {
                            System.out.println("Invalid input! Try once more!");
                        }
                    }

                    boolean flagContinue = false;
                    do {
                        System.out.println("Press -1- -------> For today");
                        System.out.println("Press -2- -------> For future");
                        System.out.print("Type here: ");

                        if (scn.hasNextInt()) {
                            reply = scn.nextInt();
                            scn.nextLine();
                            flag = false;
                        } else {
                            System.out.println("Invalid input! Try once more!");
                            scn.nextLine();
                        }

                        if (!flag) {
                            switch (reply) {
                                case 1:
                                    //getting current weather(1 day) from saved jsons

                                    //from currentData1.json
                                    List<WeatherParameter> currentWeatherAPI1 = WeatherParameter.retrieveForecastData(currentLocation, AccessKey.one.getProvider(), jsonData1, 1, tempUnit);

                                    //from currentData2.json
                                    List<WeatherParameter> currentWeatherAPI2 = WeatherParameter.retrieveForecastData(currentLocation, AccessKey.two.getProvider(), jsonData2, 1, tempUnit);

                                    //averaging values retrieved from both APIs
                                    if (currentWeatherAPI1 != null && currentWeatherAPI2 != null) {
                                        Set<WeatherParameter> currentParameters = new HashSet<>();
                                        for (int i = 0; i < currentWeatherAPI1.size(); i++) {
                                            WeatherParameter averaged = WeatherParameter.getAveragedValues(currentWeatherAPI1.get(i), currentWeatherAPI2.get(i));
                                            averaged.setLocation(currentLocation);
                                            System.out.println(averaged.toStringForecast(AccessKey.one.getProvider(), dtf));

                                            //adding averaged retrieved weatherParameter object to location object(making connection between tables)
                                            currentParameters.add(averaged);

                                            //updating location object via db
                                            locationDB.updateLocationBy(currentParameters, requestedCity);

                                            //adding to db parameters
                                            parametersDB.addParametersForCurrent(averaged);
                                        }
                                    }

                                    System.out.print("Want to continue? If Yes, press Enter! If No, type any symbol and press Enter ");
                                    flagContinue = ContinueConsoleOrNot(scn);
                                    break;
                                case 2:
                                    try {
                                        //getting location key for forecast(accuWeather)
                                        HTTPWeather connection3 = new HTTPWeather(AccessKey.three, requestedCity);
                                        JsonArray jsonData3 = connection3.getJSONArray();
                                        String locationKey = jsonData3.getJsonObject(0).getString("Key");
                                        connection3.setLocationKey(locationKey);
                                    } catch (NullPointerException e) {
                                        System.out.println("Request can not be done!");
                                        return;
                                    }

                                    long daysInDateFormat = -1;
                                    long daysInNumberFormat = -1;
                                    long finalDays;

                                    do {
                                        System.out.print("Get forecast from current date(in days, max available 4 days or by format DD/MM/YYYY): ");
                                        String inputDays = scn.nextLine();
                                        try {
                                            LocalDate indicatedDate = LocalDate.parse(inputDays, dtf);
                                            daysInDateFormat = ChronoUnit.DAYS.between(currentDate, indicatedDate);
                                            flag = false;
                                        } catch (DateTimeParseException e) {
                                            try {
                                                daysInNumberFormat = Long.parseLong(inputDays);
                                                flag = false;
                                            } catch (NumberFormatException m) {
                                                System.out.println("Invalid input! Try once more!");
                                            }
                                        }
                                    } while (flag);

                                    if (daysInNumberFormat != -1) {
                                        finalDays = daysInNumberFormat;
                                    } else {
                                        finalDays = daysInDateFormat;
                                    }

                                    if (finalDays <= 1 || finalDays > 4) {
                                        finalDays = 1;
                                    }
                                    //free API source for forecast(accuWeather)
                                    HTTPWeather connection = new HTTPWeather(AccessKey.three, requestedCity, finalDays, 0, 0);
                                    JsonObject jsonForecastData = connection.getJSON();

                                    //json writing forecast weather data to file3
                                    try {
                                        writeJSONFile(jsonForecastData, "jsonForecastData.json");
                                    } catch (Exception e) {
                                        return;
                                    }

                                    System.out.println("There is ready forecast for " + finalDays + " day(s)!");
                                    List<WeatherParameter> weatherByDays = WeatherParameter.retrieveForecastData(currentLocation, AccessKey.three.getProvider(), jsonForecastData, finalDays, tempUnit);
                                    if (weatherByDays != null) {
                                        for (int i = 0; i < weatherByDays.size(); i++) {
                                            System.out.println(weatherByDays.get(i).toStringForecast(AccessKey.three.getProvider(), dtf));
                                        }
                                    }

                                    System.out.println("\nWant to continue? If Yes, press Enter! If No, type any symbol and press Enter");
                                    flagContinue = ContinueConsoleOrNot(scn);
                                    break;
                                default:
                                    System.out.println("Number is not correct! Try once more!");
                                    flag = true;
                            }
                        }
                    } while (flag);
                    if (flagContinue) {
                        getStarted(locationDB, parametersDB);
                    }
                    break;
                case 4:
                    System.exit(0);
                    break;
                default:
                    System.out.println("Press keys from 1 to 4 for next step!");
            }
        } while (flag);
    }

    public static String getTruncated(String dateString, String charIndexSerch) {
        int indexOfT = dateString.indexOf(charIndexSerch);
        return dateString.substring(0, indexOfT);
    }

    public static Quartet<String, String, Double, Double> retrieveDataFromJSONObject(JsonObject obj) {
        Quartet<String, String, Double, Double> data = null;
        try {
            String requestedCountry = obj.getJsonObject("location").getString("country");
            String requestedRegion = obj.getJsonObject("location").getString("region");
            double requestedLatitude = Double.parseDouble(obj.getJsonObject("location").getString("lat"));
            double requestedLongitude = Double.parseDouble(obj.getJsonObject("location").getString("lon"));
            data = Quartet.with(requestedCountry, requestedRegion, requestedLongitude, requestedLatitude);
        } catch (JsonException e) {
            System.out.println("Connection problem with API provider!");
        }

        return data;
    }

    public static boolean ContinueConsoleOrNot(Scanner scn) {
        String s = scn.nextLine();
        if (s.length() == 0) {
            return true;
        } else {
            System.out.println("Exiting operation!");
        }
        return false;
    }
}
