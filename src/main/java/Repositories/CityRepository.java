package Repositories;

import Models.City;
import jakarta.persistence.*;

import java.util.List;

public class CityRepository {
    private static final EntityManagerFactory emf = Persistence.createEntityManagerFactory("weatherapi");

    public void addCity(String city) {
        EntityManager em = emf.createEntityManager();
        EntityTransaction et = null;

        try {
            et = em.getTransaction();
            et.begin();

            City current = new City();
            current.setCity(city);

            em.persist(current);
            et.commit();
        } catch (Exception e) {
            if (et != null) {
                et.rollback();
            }
        } finally {
            em.close();
        }
    }

    public boolean isCityPresent(String city) {
        EntityManager em = emf.createEntityManager();
        String sql = "SELECT * FROM city WHERE city = :city";
        Query query = em.createNativeQuery(sql, City.class);
        query.setParameter("city", city);
        List<City> resultList = query.getResultList();
        for (City current:
             resultList) {
            if(current.getCity().equals(city)){
                return true;
            }
        }
        return false;
    }
}
