package Repositories;

import Models.City;
import Models.Location;
import Models.WeatherParameters.WeatherParameter;
import jakarta.persistence.*;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class LocationRepository {
    private static final EntityManagerFactory emf = Persistence.createEntityManagerFactory("weatherapi");

    public void addLocation(String city, String country, String region, double longitude, double latitude) {
        EntityManager em = emf.createEntityManager();
        EntityTransaction et = null;

        try {
            et = em.getTransaction();
            et.begin();

            Location current = new Location();
            current.setCity(city);
            current.setCountry(country);
            current.setRegion(region);
            current.setLongitude(longitude);
            current.setLatitude(latitude);

            em.persist(current);
            et.commit();
        } catch (Exception e) {
            if (et != null) {
                et.rollback();
            }
        } finally {
            em.close();
        }
    }

    public List<Location> getAll() {
        EntityManager em = emf.createEntityManager();
        EntityTransaction et = null;
        Query query = em.createQuery("SELECT e FROM Location e");
        List<Location> results = query.getResultList();
        em.close();
        return results;
    }

    public boolean ifExists(String city) {
        EntityManager em = emf.createEntityManager();
        EntityTransaction et = null;
        boolean result = false;

        try {
            et = em.getTransaction();
            et.begin();

            Query query = em.createQuery("SELECT COUNT(e) FROM Location e WHERE e.city = :value");
            query.setParameter("value", city);

            Long count = (Long) query.getSingleResult();

            if (count > 0) {
                result = true;
            }

            et.commit();
        } catch (Exception e) {
            if (et != null) {
                et.rollback();
            }
        } finally {
            em.close();
        }
        return result;
    }

    public Location getLocation(String city) {
        EntityManager em = emf.createEntityManager();
        EntityTransaction et = null;
        Location location = null;

        try {
            et = em.getTransaction();
            et.begin();

            String query = "SELECT u FROM Location u WHERE u.city = :name";
            TypedQuery<Location> typedQuery = em.createQuery(query, Location.class);
            typedQuery.setParameter("name", city);

            location = typedQuery.getSingleResult();

            et.commit();
        } catch (Exception e) {
            if (et != null) {
                et.rollback();
            }
        } finally {
            em.close();
        }
        return location;
    }

    public void updateLocationBy(Set<WeatherParameter> weatherParameters, String city) {
        EntityManager em = emf.createEntityManager();
        EntityTransaction et = null;
        Location location = null;

        try {
            et = em.getTransaction();
            et.begin();
            String query = "SELECT u FROM Location u WHERE u.city = :name";
            TypedQuery<Location> typedQuery = em.createQuery(query, Location.class);
            typedQuery.setParameter("name", city);

            location = typedQuery.getSingleResult();
            location.setWeatherParameters(weatherParameters);
            em.persist(location);
            et.commit();
        } catch (Exception e) {
            if (et != null) {
                et.rollback();
            }
        } finally {
            em.close();
        }
    }
}
