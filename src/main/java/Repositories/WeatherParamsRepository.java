package Repositories;

import Models.Location;
import Models.WeatherParameters.DayTime;
import Models.WeatherParameters.LocalTemp;
import Models.WeatherParameters.NightTime;
import Models.WeatherParameters.WeatherParameter;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.Persistence;

import java.time.LocalDate;

public class WeatherParamsRepository {
    private static final EntityManagerFactory emf = Persistence.createEntityManagerFactory("weatherapi");

    public void addParametersForCurrent(WeatherParameter currentParams) {
        EntityManager em = emf.createEntityManager();
        EntityTransaction et = null;

        try {
            et = em.getTransaction();
            et.begin();

            em.persist(currentParams);
            et.commit();
        } catch (Exception e) {
            if (et != null) {
                et.rollback();
            }
        } finally {
            em.close();
        }
    }

}
