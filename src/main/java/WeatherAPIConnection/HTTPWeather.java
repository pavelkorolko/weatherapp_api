package WeatherAPIConnection;

import AccessKeys.AccessKey;
import top.jfunc.json.JsonArray;
import top.jfunc.json.JsonObject;
import top.jfunc.json.impl.JSONArray;
import top.jfunc.json.impl.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class HTTPWeather {
    private HttpURLConnection connection;
    private BufferedReader reader;
    private StringBuilder content;
    private String line;
    private String source;
    private int status;
    private static long locationKey;
    private URL url;

    public HTTPWeather(AccessKey key, String city) {
        try {
            content = new StringBuilder();
            if (AccessKey.one == key) {
                //weatherStack
                source = "http://api.weatherstack.com/current" +
                        "?access_key=" + key.getValue() +
                        "&query=" + city;
            } else if (AccessKey.two == key) {
                //openWeatherMap
                source = "https://api.openweathermap.org/data/2.5/weather?q=" + city + "&appid=" + key.getValue() + "&units=metric";
            }else if(AccessKey.three == key){
                //accuweather
                source = "http://dataservice.accuweather.com/locations/v1/cities/search?apikey=" + key.getValue() + "&q=" + city;
            }

            url = new URL(source);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            status = connection.getResponseCode();

        } catch (Exception e) {
            System.out.println("HTTP status connection: " + status);
        }
    }

    public HTTPWeather(AccessKey key, String city, long forecastDays, double lat, double lon) {
        try {
            content = new StringBuilder();
            if (AccessKey.one == key) {
                //weatherStack
                source = "http://api.weatherstack.com/forecast" +
                        "?access_key=" + key.getValue() +
                        "&query=S" + city +
                        "&forecast_days=" + forecastDays;
            } else if (AccessKey.two == key) {
                //openWeatherMap
                source = "api.openweathermap.org/data/2.5/forecast/daily?lat={" + lat + "}&lon={" + lon + "}&cnt={"
                        + forecastDays + "}&appid={" + key.getValue() + "}";
            }else if(AccessKey.three == key){
                //accuweather
                source = "http://dataservice.accuweather.com/forecasts/v1/daily/5day/" + locationKey + "?apikey=" + key.getValue();
            }

            url = new URL(source);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            status = connection.getResponseCode();

        } catch (Exception e) {
            System.out.println("HTTP status connection: " + status);
        }
    }

    public StringBuilder getData() {
        try {
            reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            while ((line = reader.readLine()) != null) {
                content.append(line);
            }
            reader.close();
        } catch (Exception e) {
            System.out.println("HTTP status connection: " + status);
        }
        return content;
    }

    public void setLocationKey(String locationKey) {
        this.locationKey = Long.parseLong(locationKey);
    }

    public JsonObject getJSON(){
        StringBuilder requestedForecastData = this.getData();
        return new JSONObject(requestedForecastData.toString());
    }

    public JSONArray getJSONArray(){
        StringBuilder requestedData3 = this.getData();
        return new JSONArray(requestedData3.toString());
    }
}
