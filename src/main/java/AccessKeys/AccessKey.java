package AccessKeys;

public enum AccessKey {
    one(1, "4282a84e00e15a0807a9ebd10b844afe", "WeatherStack"),
    two(2, "c95bfe4fe738c7ac25facba17cb86b62", "OpenWeatherMap"),
    three(3, "Uwswu1nRfE9cxfAt2vcOWzAYD3fioOAM", "AccuWeather");

    private final int id;
    private final String value;
    private final String provider;

    AccessKey(int id, String value, String provider) {
        this.id = id;
        this.value = value;
        this.provider = provider;
    }

    public String getValue() {
        return value;
    }

    public String getProvider() {
        return provider;
    }
}
